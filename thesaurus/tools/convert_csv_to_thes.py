﻿import os
import sys
from ar_ctype import *

#-------------------------
#  Parameters
SEPARATOR=";"
FILE_DATA=u"data/syno.csv"
My_limit=10000;

NB_FIELD_MAX=4
NB_FIELD_MIN=3



#---------------------------------
# main readfile
#---------------------------------
def import_file():
	filename=FILE_DATA;
	try:
		fl=open(filename);
	except:
		print " Error :No such file or directory: %s" % filename
		sys.exit(0)
	line=fl.readline().decode("utf");
	text=u""
	tuple_table=[];
	nb_field=NB_FIELD_MAX;
	while line :
		if not line.startswith("#"):
			line=chomp(line)
			liste=line.split(SEPARATOR);
			if len(liste)<NB_FIELD_MIN:
			  print len(tuple_table),line;
			  exit();
			tuple_table.append(liste);
		line=fl.readline().decode("utf");
	fl.close();



	id=0;

	limit=My_limit;
	table_syno={};

	for i in range(min(limit,len(tuple_table))):
		id=i+1;# to avoid zero
#------------------------------------------------------
# treat data
#------------------------------------------------------
		tup=tuple_table[i];

		word=tup[0].strip();
		type_word=tup[1].strip();
		syno_row={}
		syno_row[0]=word.strip();
		syno_row[1]=tup[2].strip();
		if len(tup)>=4 and tup[3]!="":
		    syno_row[2]=tup[3].strip();
		if len(tup)>=5 and tup[4]!="": syno_row[3]=tup[4].strip();
		type_normalized=normalize_searchtext(type_word);
		if type_normalized==u"اسم":
		    type_word=u"اسم"
		elif type_normalized==u"فعل":
		    type_word=u"فعل"
		elif type_normalized==u"مصدر":
		    type_word=u"مصدر"
		else:
		    type_word="unknown"

		for i in range(len(syno_row)):

		    syno_set=[]
		    for j in range(len(syno_row)):
		        if j!=i:

        		    syno_set.append(syno_row[j])
		    syno_key=ar_strip_marks_keepshadda(syno_row[i])
		    if not table_syno.has_key(syno_key):
		        table_syno[syno_key]=[]
		    table_syno[syno_key].append((type_word,syno_set));

# create thesaurus format
	#for i in range(min(limit*3,len(table_syno))):
	for word in table_syno.keys():
	    print u"%s|%d"%(word,len(table_syno[word]));
	    for syno_set in table_syno[word]:
	        print u"(%s)|%s"%(syno_set[0],"|".join(syno_set[1]))






if __name__ == "__main__":
#To load info from csv file to thesaurus format
    import_file();






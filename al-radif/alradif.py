#! /usr/bin/python
# -*- coding: UTF-8 -*-
"""
webQamoos - an Arabic-English disctionary to demonistrate Okasha web framework
we are using same DB as Qamoos by abdelrahman ghanem <abom.jdev@gamil.com>
but we did not use his code.

Copyright © 2009, Muayyad Alsadi <alsadi@ojuba.org>
    Released under terms of Waqf Public License.
    This program is free software; you can redistribute it and/or modify
    it under the terms of the latest version Waqf Public License as
    published by Ojuba.org.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

    The Latest version of the license can be found on
    "http://waqf.ojuba.org/license"

"""
import sys, os, os.path, re
from glob import glob
import sqlite3
from okasha.baseWebApp import *
from okasha.utils import fromFs, toFs

class webApp(baseWebApp):
  sqlByLang={'en':'SELECT en,ar FROM wordlist WHERE en like "%s%%" limit 100',
    'ar':'SELECT en,ar FROM wordlist WHERE ar like "%s%%" limit 100'}
  sqlByDict={
        'synonyms': u'SELECT vocalized as myword, syno_set_vocalized as myset ,word_type as mytype FROM synonyms WHERE word like "%s%%"  or syno_set like "%%%s%%" limit 100',
        'antonyms': u'SELECT vocalized as myword, anto_set_vocalized as myset ,word_type as mytype FROM antonyms WHERE word like "%s%%"  or anto_set like "%%%s%%" limit 100',
        'qawafi'  : u'SELECT vocalized as myword, inversed as myset ,word_type as mytype FROM qawafi WHERE inversed like "%s%%"  limit 100',
        'plural'  : u'SELECT vocalized as myword, plural_vocalized as myset ,word_type as mytype FROM plural WHERE word like "%s%%"  or plural like "%%%s%%" limit 100',

        #3:u'SELECT word as myword, anto_set_vocalized as myset ,word_type as mytype FROM antonyms WHERE word like "%s%%"  or anto_set like "%%%s%%" limit 100',

        }

  alphanumre=re.compile(u'^[\w ]+$',re.U)
  def __init__(self, db_dir,*args, **kw):
    self.db_dir=db_dir
    self.db_l=map(lambda s:os.path.basename(s)[:-3],glob(db_dir+"/*.db"))
    baseWebApp.__init__(self,*args, **kw)

  def _root(self, rq, *args):
    raise redirectException(rq.script+'/main/')

  @expose(percentTemplate,["main.html"])
  def main(self, rq, *args):
    return {'script':rq.script}

  @expose(jsonDumps)
  def lsDb(self, rq, *args):
    return list(enumerate(self.db_l))

  @expose(jsonDumps)
  def search(self, rq, *args):
    try: w=(rq.q.getfirst('w','')).strip().decode('utf8')
    except UnicodeDecodeError: raise forbiddenException()
    # for security reasons we check w against alphanum
    w=re.sub(u'[\u064b-\u0652]','',w)
    if not w or self.alphanumre.match(w)==None: raise forbiddenException()

    lang=rq.q.getfirst('lang','en')
##    if lang not in self.sqlByLang.keys(): raise forbiddenException()
    try: db_i=(rq.q.getfirst('db','synonyms')).strip().decode('utf8')
    except ValueError: db_i='synonyms';
##    if db_i<0 or db_i>=len(self.db_l): raise forbiddenException()
##    db_fn=os.path.join(self.db_dir,self.db_l[db_i]+'.db')
    db_fn=os.path.join(self.db_dir,db_i+'.db')
    cn=sqlite3.connect(db_fn)
    cn.row_factory=sqlite3.Row
    c=cn.cursor()
   # r=c.execute(self.sqlByLang[lang] % w).fetchall()
    if db_i in ("qawafi"):
        # search by inversed word
        r=c.execute(self.sqlByDict[db_i] % w[::-1]).fetchall()
    else:
        r=c.execute(self.sqlByDict[db_i] % (w,w)).fetchall()
    cn.close()
    return map(lambda d: dict(d),r)

if __name__ == '__main__':
    # this requires python-paste package
    import logging
    from paste import httpserver

    myLogger=logging.getLogger('MyTestWebApp')
    h=logging.StreamHandler() # in production use WatchedFileHandler or RotatingFileHandler
    h.setFormatter(logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s"))
    myLogger.addHandler(h)
    myLogger.setLevel(logging.INFO) # in production use logging.INFO
    d=fromFs(os.path.dirname(sys.argv[0]))
    app=webApp(
      os.path.join(d,u'db'),
      os.path.join(d,u'templates'),
      staticBaseDir={u'/_files/':os.path.join(d,u'files')}
    );
    # for options see http://pythonpaste.org/modules/httpserver.html
    httpserver.serve(app, host='0.0.0.0', port='8080')


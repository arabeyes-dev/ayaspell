#!/usr/bin/python
# -*- coding=utf-8 -*-

from ar_ctype import *
##from classverb import *
import sys,re,string
import sys, getopt, os
scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
def usage():
# "Display usage options"
	print "(C) CopyLeft 2009, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-V | --version]\tprogram version"
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname
	print "\t[-a | --all ]\t\tConjugate in all tenses"
	print "\t[-i | --imperative]\tConjugate in imperative"
	print "\t[-F | --future]\t\tconjugate in the present and the future"
	print "\t[-p | --past]\t\tconjugate in the past"
	print "\t[-v | --passive]\tpassive form";
	print "\r\nThis program is licensed under the GPL License\n"

def grabargs():
#  "Grab command-line arguments"
	all = False;
	future=False;
	past=False;
	passive=False;
	imperative=False;
	fname = ''

	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hVvnaiFpi:f:",
                               ["help", "version","imperative", "passive", "past","all",
                                "future",  "file="],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-V", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-v", "--passive"):
			passive = True
		if o in ("-f", "--file"):
			fname = val
		if o in ("-F", "--future"):
			future = True
		if o in ("-a", "--all"):
			all=True;
		if o in ("-p", "--past"):
			past =True;
		if o in ("-i","-imperative"):
			imperative=True;
	return (fname,all,future,past,passive,imperative)

def main():
	filename,all,future,past,passive,imperative= grabargs()
	try:
		fl=open(filename);
	except:
		print " Error :No such file or directory: %s" % filename
		sys.exit(0)
	text=readfilelines(filename);

	if not text:sys.exit(0);
	else :
##	    print text;
	    print "---------------------"
	    print unshaping_text(text);
if __name__ == "__main__":
  main()








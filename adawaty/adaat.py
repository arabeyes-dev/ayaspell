﻿from core.ar_ctype import *
##from ar_trans import *
from core.number import *
def DoAction(text,action):
    if action=="DoNothing":
        return text;
    elif action=="StripHarakat":
        return ar_strip_marks_keepshadda(text);
    elif action=="CsvToData":
        return csv_to_python_table(text);
    elif action=="Romanize":
        return romanize(text);
    elif action=="NumberToLetters":
        return numberToLetters(text);
    elif action=="LightStemmer":
        return lightStemmer(text);
    elif action=="Tokenize":
        return token_text(text);
    elif action=="Poetry":
        return justify_poetry(text);
    else:
        justify_poetry
        return text;

#----------------------------
# Convert CSV text to python syntax Array
# return Text
#----------------------------
def csv_to_python_table(text):
    lines=text.splitlines()
    if u'' in lines: lines.remove(u'');
    resultText=""

    if len(lines)>1:
        tablename=lines[0];
        if tablename=="":
            tablename="#Table";
        else :
            tablename=tablename.split()[0];
        # if there only two lines, the array is a list
        if len(lines)==2:
            fieldsnames=lines[1].split("\t");
            for i in range(len(fieldsnames)):
                fieldsnames[i]="'%s'"%fieldsnames[i].strip();
            resultText+=tablename+"=("+",".join(fieldsnames)+u")\n";
        else:
            resultText+=tablename+u"={};\n";
            fieldsnames=lines[1].split("\t");
            for i in range(len(fieldsnames)):
                fieldsnames[i]=fieldsnames[i].strip();
                resultText+=tablename+u"['%s']={}\n"%fieldsnames[i];

            if len(lines)==3:
                for line in lines[1:]:
                    line=line.strip();
                    fields=line.split("\t")
                    for i in range(len(fields)):
                        fields[i]=fields[i].strip();
            ##            fields[i]=re.sub('\\',''',fields[i])
                        fields[i]=re.sub("'","\\'",fields[i])
                        fields[i]=re.sub("\"","\\\"",fields[i])
                    resultText+=tablename+u"[u'%s']={}\n"%fields[0];
                    for i in range(0,len(fields)):
                        if i< len(fieldsnames):
                            fieldname=fieldsnames[i];
                        else:
                            fieldname=u"Field#%d"%i;
                            fieldsnames.append(fieldname);
                            resultText+=tablename+u"[u'%s']={}\n"%fieldname;

                        resultText+= tablename+u"[u'%s']=u'%s'"%(fieldname,fields[i])+";\n"

            else:
                for line in lines[1:]:
                    line=line.strip();
                    fields=line.split("\t")
                    for i in range(len(fields)):
                        fields[i]=fields[i].strip();
            ##            fields[i]=re.sub('\\',''',fields[i])
                        fields[i]=re.sub("'","\\'",fields[i])
                        fields[i]=re.sub("\"","\\\"",fields[i])

                    for i in range(1,len(fields)):
                        if i< len(fieldsnames):
                            fieldname=fieldsnames[i];
                        else:
                            fieldname=u"Field#%d"%i;
                            fieldsnames.append(fieldname);
                            resultText+=tablename+u"[u'%s']={}\n"%fieldname;

                        resultText+= tablename+u"[u'%s'][u'%s']='%s'"%(fieldname,fields[0],fields[i])+";\n"

    return resultText

#-----------------------------------------
# Convert Arabic into Latin using a code representaton
#-----------------------------------------

def romanize(text,code="ISO"):
	textcoded=u"";
	if ArabicRomanizationTable.has_key(code):
		for c in text:
			if ArabicRomanizationTable[code].has_key(c):
				print "1";
				if explicated:
					textcoded+="("+c+")"
					print "2"
				textcoded+=ArabicRomanizationTable[code][c]
				print "3"
			else:
				textcoded+="*"
	else:
		textcoded=text;
		print "4"
	return textcoded;

def numberToLetters(text):
##    number=int(text);
    text=text.strip();
    ar=ArNumbers();
    return ar.int2str(text);

#---------------------------------
#
#LightStemming unsing Tashaphyne
#--------------------------------
import tashaphyne

def lightStemmer(text):
    result=[];
    als=tashaphyne.ArabicLightStemmer();
    word_list=alstokenize(text)
    for word in word_list:
        listseg= als.segment(word);
##        print word.encode("utf8"),listseg
        affix_list=als.get_affix_list();
        for affix in affix_list:
            result.append({'word':word,'prefix':affix['prefix'],'stem':affix['stem'],
                           'suffix':affix['suffix'],'root':affix['root'],'type':'-'}
                          );

    return result;

def token_text(text):
    tasha=tashaphyne.ArabicLightStemmer();
    return tasha.tokenize(text);
def justify_poetry(text):
    lines=text.splitlines();
    if u'' in lines: lines.remove(u"")
    rows=[];
    for line in lines:
        partlist=line.strip().split("\t");
        if u'' in partlist: partlist.remove(u"");
        if len(partlist)==2:
            rows.append(partlist);
    return rows;
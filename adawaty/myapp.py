﻿#! /usr/bin/python
# -*- coding: UTF-8 -*-
"""
this is my first okasha web app
"""
from okasha.baseWebApp import *
from ar_ctype import *
class MyApp(baseWebApp):
  def __init__(self, *args, **kw):
    baseWebApp.__init__(self,*args, **kw)
  @expose()
  def _root(self, rq, *args):
    return """<html><body>
<img src="%s/media/ar-php.jpg"/>
<h1>welcome to my first okasha application</h1>
</body></html>""" % rq.script



  @expose()
  def hello(self, rq, *args):
    n=rq.q.getfirst('n','world').decode('utf-8')
    text=rq.q.getfirst('text','').decode('utf-8')
    text=ar_strip_marks_keepshadda(text);
    return """<html dir="rtl"><body>
<h1>Hello, {0}!</h1>
<form name="MyForm">
Please enter name:
<input name="n" type="text" value="{0}"></input>
<textarea name="text">السلام عليكم
</textarea>
<input type="submit" value='أرسل'/>
<input type="reset" value='امسح'/>
</form>
<hr/>
<h2>النتائج</h2>
<div>
<pre>
{1}
</pre>
<div>
</body></html>""".format(n,text.encode('utf8'))

#----------------------------------------------
#
#-----------------------------------------------
  @expose(percentTemplate,["tmp.html"])
  def tmp(self, rq, *args):
    """
    this is how you can use simple % formatting templates,
    just return a dictionary, and it will be applied to the named template.
    to test it visit
      http://localhost:8080/tmp/some/args/?id=5
      http://localhost:8080/tmp/err/raised/?id=5
    """
    if args and args[0]=='err': raise forbiddenException()
    n=rq.q.getfirst('n','world').decode('utf-8')
    text=rq.q.getfirst('text','').decode('utf-8')
    resulttext=ar_strip_marks_keepshadda(text);
    return {
      'title':u'الأدوات',
      'DefaultText':text,
      'ResultText':resulttext,
      'script':rq.script,
      'key1':'val1','key2':'val2','args':'/'.join(args)
      }


  @expose(jsonDumps)
  def ajaxGet(self, rq, *args):
    """
    this is an example of using ajax/json
    to test it visit http://localhost:8080/ajaxGet"
    """
    return {'result':"Taha",}
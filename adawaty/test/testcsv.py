﻿from adaat import *
text=u"""
ArabicRomanizationTable
Letter 	Unicode 	Name 	SATTS 	UNGEGN 	ALA-LC 	DIN 	ISO 	ISO/R 	Qalam 	SAS 	SM 	Buckwalter 	IPA 	BATR 	ArabTeX 	OnlineScript
ﺀ 	0621 	hamza 	E 	ʼ, — 	—, ’ 	ʾ 	ˈ, ˌ 	—, ’ 	' 	ʾ 	' 	' 	/ʔ/ 	e 	' 	2
ﺍ 	0627 	ʼalif 	A 		ā 	ʾ 	ā 	aa 	a, i, u; ā 	aa 	A 	/a(ː)/ 	aa or A 	a 	a
ﺏ 	0628 	bāʼ 	B 	b 	b 	b 	b 	b 	b 	/b/ 	b 	b 	b
ﺕ 	062A 	tāʼ 	T 	t 	t 	t 	t 	t 	t 	/t/ 	t 	t 	t
ﺙ 	062B 	ṯāʼ 	C 	th 	ṯ 	th 	ṯ 	ç 	v 	/θ/ 	c 	_t 	th
ﺝ 	062C 	ǧīm, jīm, gīm 	J 	j 	ǧ 	j 	ŷ 	j 	j 	/d͡ʒ/ / /ɡ/ 	j 	^g 	j/g
ﺡ 	062D 	ḥāʼ 	H 	ḩ 	ḥ 	ḥ 	H 	ḥ 	ḥ 	H 	/ħ/ 	H 	.h 	7
ﺥ 	062E 	ḫāʼ 	O 	kh 	ḫ 	ẖ/ẖ 	kh 	j 	x 	x 	/x/ 	K 	_h 	7'/kh
ﺩ 	062F 	dāl 	D 	d 	d 	d 	d 	d 	d 	/d/ 	d 	d 	d
ﺫ 	0630 	ḏāl 	Z 	dh 	ḏ 	dh 	ḏ 	đ 	* 	/ð/ 	z' 	_d 	th/z
ﺭ 	0631 	rāʼ 	R 	r 	r 	r 	r 	r 	r 	/r/ 	r 	r 	r
ﺯ 	0632 	zāy 	 ; 	z 	z 	z 	z 	z 	z 	/z/ 	z 	z 	z
ﺱ 	0633 	sīn 	S 	s 	s 	s 	s 	s 	s 	/s/ 	s 	s 	s
ﺵ 	0634 	šīn 	 : 	sh 	š 	sh 	š 	š 	$ 	/ʃ/ 	x 	^s 	sh/ch
ﺹ 	0635 	ṣād 	X 	ş 	ṣ 	ṣ 	S 	ṣ 	ṣ 	S 	/sˁ/ 	S 	.s 	s/S
ﺽ 	0636 	ḍād 	V 	ḑ 	ḍ 	ḍ 	D 	ḍ 	ḍ 	D 	/dˁ/ 	D 	.d 	d/D
ﻁ 	0637 	ṭāʼ 	U 	ţ 	ṭ 	ṭ 	T 	ṭ 	ṭ 	T 	/tˁ/ 	T 	.t 	T/t/6
ﻅ 	0638 	ẓāʼ 	Y 	z̧ 	ẓ 	ẓ 	Z 	ẓ 	đ̣ 	Z 	/ðˁ/ 	Z 	.z 	Z/z/6'
ﻉ 	0639 	ʻayn 	` 	ʻ 	ʿ 	` 	ʿ 	ř 	E 	/ʕ/ 	E 	` 	3
ﻍ 	063A 	ġayn 	G 	gh 	ġ 	ḡ 	gh 	g 	ğ 	g 	/ɣ/ 	g 	.g 	gh/3'
ﻑ 	0641 	fāʼ 	F 	f 	f 	f 	f 	f 	f 	/f/ 	f 	f 	f
ﻕ 	0642 	qāf 	Q 	q 	q 	q 	q 	q 	q 	/q/ 	q 	q 	q/2/k
ﻙ 	0643 	kāf 	K 	k 	k 	k 	k 	k 	k 	/k/ 	k 	k 	k
ﻝ 	0644 	lām 	L 	l 	l 	l 	l 	l 	l 	/l/ 	l 	l 	l
ﻡ 	0645 	mīm 	M 	m 	m 	m 	m 	m 	m 	/m/ 	m 	m 	m
ﻥ 	0646 	nūn 	N 	n 	n 	n 	n 	n 	n 	/n/ 	n 	n 	n
ﻩ 	0647 	hāʼ 	~ 	h 	h 	h 	h 	h 	h 	/h/ 	h 	h 	h
ﻭ 	0648 	wāw 	W 	w 	w 	w 	w; ū 	w; o 	w 	/w/, /uː/ 	w or uu 	w 	w
ﻱ 	064A 	yāʼ 	I 	y 	y 	y 	y; ī 	y; e 	y 	/j/, /iː/ 	y or ii 	y 	y/i
ﺁ 	0622 	ʼalif madda 	AEA 	ā 	ā, ʼā 	ʾā 	ʾâ 	ā, ʾā 		ā 	'aa 	| 	/ʔaː/ 	eaa 	'A 	a/aa
ﺓ 	0629 	tāʼ marbūṭa 	@ 	h, t 	h, t 	ẗ 	h, t 	h, t 	t; — 	ŧ 	p 	/a/, /at/ 	t' 	T 	a/ah
ﻯ 	0649 	ʼalif maqṣūra 	/ 	y 	ā 	ỳ 		ae 	à 	à 	Y 	/aː/ 	aaa 	_A 	a/aa
ﻻ 	FEFB 	lām ʼalif 	LA 	lā 	lā 	laʾ 	lā 	la 	lʾ; lā 	laa 		/lː/ 	laa 	lA 	la
ال 		ʼalif lām 	AL 	al- 	al- 	ʾˈal 	al- 	al 	al- 	al-; ál- 		var. 	Al- 	al- 	l-/double consonant
"""
resulttext=csv_to_python_table(text);
print resulttext.encode("utf8");
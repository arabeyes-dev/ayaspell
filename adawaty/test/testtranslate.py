﻿print ord("A")
table="*"*64+"#"*192;
print table
print "Taha.Zerrouki@".translate(table);
print "---------------------"
import  re
print ord(u"ع")
table=u"*"*64+u"#"*192;
print table
print u"طه رزقي..@".translate(table);
mytable={}
for i in range(256):
    mytable[i]=chr(i);
    print chr(i)
##print mytable
MorseCharCodes={}
MorseCharCodes[u"أ"]=u". _";
MorseCharCodes[u"ء"]=u". _";
MorseCharCodes[u"ئ"]=u". _";
MorseCharCodes[u"ا"]=u". _";
MorseCharCodes[u"آ"]=u". _";
MorseCharCodes[u"ـ"]=u"";
MorseCharCodes[u"إ"]=u". _";
MorseCharCodes[u"ب"]=u"_ . . .";
MorseCharCodes[u"ت"]=u"_";
MorseCharCodes[u"ث"]=u"_ . _ .";
MorseCharCodes[u"ج"]=u". _ _ _";
MorseCharCodes[u"ح"]=u". . . .";
MorseCharCodes[u"خ"]=u"_ _ _";
MorseCharCodes[u"د"]=u"_ . .";
MorseCharCodes[u"ذ"]=u"_ _ . .";
MorseCharCodes[u"ر"]=u". _ .";
MorseCharCodes[u"ز"]=u"_ _ _ .";
MorseCharCodes[u"س"]=u". . .";
MorseCharCodes[u"ش"]=u"_ _ _ _";
MorseCharCodes[u"ص"]=u"_ . . _";
MorseCharCodes[u"ض"]=u". . . _";
MorseCharCodes[u"ط"]=u". . _";
MorseCharCodes[u"ظ"]=u"_ . _ _";
MorseCharCodes[u"ع"]=u". _ . _";
MorseCharCodes[u"غ"]=u"_ _ .";
MorseCharCodes[u"ف"]=u". . _ .";
MorseCharCodes[u"ق"]=u"_ _ . _";
MorseCharCodes[u"ك"]=u"_ . _";
MorseCharCodes[u"ل"]=u". _ . .";
MorseCharCodes[u"م"]=u"_ _";
MorseCharCodes[u"ن"]=u"_ .";
MorseCharCodes[u"ه"]=u". ._ . .";
MorseCharCodes[u"ة"]=u". ._ . .";
MorseCharCodes[u"و"]=u". _ _";
MorseCharCodes[u"ؤ"]=u". _ _";
MorseCharCodes[u"ي"]=u". .";
MorseCharCodes[u"ى"]=u". .";
MorseCharCodes[u"1"]=u". _ _ _ _";
MorseCharCodes[u"2"]=u". . _ _ _";
MorseCharCodes[u"3"]=u". . . _ _";
MorseCharCodes[u"4"]=u". . . . _";
MorseCharCodes[u"5"]=u". . . . .";
MorseCharCodes[u"6"]=u"_ . . . .";
MorseCharCodes[u"7"]=u"_ _ . . .";
MorseCharCodes[u"8"]=u"_ _ _ . .";
MorseCharCodes[u"9"]=u"_ _ _ _ .";
MorseCharCodes[u"0"]=u"_ _ _ _ _";

def morse(text,explicated=False):
    textcoded=u"";
    for c in text:
        if MorseCharCodes.has_key(c):
            if explicated:
                textcoded+="("+c+")"
            textcoded+=MorseCharCodes[c]
        else:
            textcoded+=c
    return textcoded;
source="abcdefghigklmno"
target=source.upper();
word=u"طه زروقي";
print morse(word);

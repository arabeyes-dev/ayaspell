﻿
def token_text(text):
    lines=text.splitlines();
    if u'' in lines: lines.remove(u"")
    words=[];
    for line in lines:
        wordlist=line.strip().split();
        if u'' in wordlist: wordlist.remove(u"");
        for w in wordlist:
            w=w.strip();
            if w not in words:
                words.append(w)
    return words;


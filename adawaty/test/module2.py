﻿##import re
def csv_to_python_table(text):
    lines=text.splitlines()
    lines.remove(u'');
    tablename=lines[0];
    if tablename=="":
        tablename="#Table";
    fieldsnames=lines[1].split("\t");
    for i in range(len(fieldsnames)):
        fieldsnames[i]=fieldsnames[i].strip();
    resultText=tablename+u"={};\n";
    for line in lines[1:]:
        line=line.strip();
        fields=line.split("\t")
        for i in range(len(fields)):
            fields[i]=fields[i].strip();
##            fields[i]=re.sub('\\',''',fields[i])
            fields[i]=re.sub("'","\\'",fields[i])
            fields[i]=re.sub("\"","\\\"",fields[i])
        resultText+=tablename+u"['%s']={}\n"%fields[0];
        for i in range(1,len(fields)):
            if i< len(fieldsnames):
                fieldname=fieldsnames[i];
            else:
                fieldname=u"Field#%d"%i;
                fieldsnames.append(fieldname);

            resultText+= tablename+u"['%s']['%s']='%s'"%(fields[0],fieldname,fields[i])+";\n"

    return resultText

txt=u"""
translatira
Letter 	Unicode 	Name 	SATTS 	UNGEGN 	ALA-LC 	DIN 	ISO 	ISO/R 	Qalam 	SAS 	SM 	Buckwalter 	IPA 	BATR 	ArabTeX 	OnlineScript
ﺀ 	0621 	hamza 	E 	ʼ, — 	—, ’ 	ʾ 	ˈ, ˌ 	—, ’ 	' 	ʾ 	' 	' 	/ʔ/ 	e 	' 	2
ﺍ 	0627 	ʼalif 	A 		ā 	ʾ 	ā 	aa 	a, i, u; ā 	aa 	A 	/a(ː)/ 	aa or A 	a 	a
ﺏ 	0628 	bāʼ 	B 	b 	b 	b 	b 	b 	b 	/b/ 	b 	b 	b
"""
result=csv_to_python_table(txt);
print result;
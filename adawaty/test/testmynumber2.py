﻿#!/usr/bin/python
# -*- coding=utf-8 -*-
from math import *
from string import *
from random import *
# قراءة العدد المدخل
tab_millier=['',u"ألف",u"مليون",
u"مليار",u"بليون",u"بليار",u"تريليون",u"كودريلوين"]
tab_cent=[u"",u"مئة",u"مئتان",u"ثلاثمئة",u"أربعمئة",u"خمسمئة",u"ستمئة",u"سبعمئة",u"ثمانمئة",u"تسعمئة"]
tab_tens=[u"",u"عشرة",u"عشرون",u"ثلاثون",u"أربعون",u"خمسمون",u"ستمون",u"سبعون",u"ثمانمون",u"تسعون"]
tab_units=[u"",u"واحد",u"اثنان",u"ثلاثة",u"أربعة",u"خمسة",u"ستة",u"سبعة",u"ثمانية",u"تسعة"]
def cent(number,millier=''):
	if number>=1000:number=number %1000;
	cents=int(number/100)
	number=number%100;
	tens=int(number/10)
	units=int(number%10);
	words=[];
	if cents>0:
		words.append(tab_cent[cents]);
	if units>0 and tens==1:
	    words.append(tab_units[units]+" "+u"عشر");
	else:
	    if units>0 :
		  words.append(tab_units[units]);
	    if tens>0:
		words.append(tab_tens[tens]);
	return join(words,u"  و") +u" "+millier;

def log1000(number):
	return log(number)/log(1000);
def funct(number):
	if number<1000:
		return cent(number);
#		return number;
	else :
		lg=int(log1000(number))
		number_i=int(number/pow(1000,lg));
		cent(number_i,tab_millier[lg]);
		number=number % pow(1000,lg);
#		print "number rest",number;
		u=cent(number_i,tab_millier[lg]);
		c=funct(number);
		if c!="" and u!="":return u+u"  و"+c;
		elif c!="" and u=="":return c;
		elif c=="" and u!="":return u;
#rn 		cent(number_i,tab_millier[lg]), funct(number);
number=145245000786;
##number=14
words=funct(number);
print words;
##for number in range(21):
##    words=funct(number);
##    print words;


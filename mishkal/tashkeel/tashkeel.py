﻿#!/usr/bin/python
# -*- coding=utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        tashkeel
# Purpose:     Arabic automatic vocalization.
#
# Author:      Taha Zerrouki (taha.zerrouki[at]gmail.com)
#
# Created:     31-10-2011
# Copyright:   (c) Taha Zerrouki 2011
# Licence:     GPL
#-------------------------------------------------------------------------------

import re
import pyarabic.araby as araby
import tashkeel_const
import qalsadi as aranalex#.analex
#import aranalex.analex 
import aranasyn.anasyn 
import collocations
#import wordfreqdictionaryclass
# arabicWordFreq={
# u'عَرَبٌ':10,
# }
class TashkeelClass:
	"""
        Arabic Tashkeel Class
	"""

	def __init__(self):
		# to display internal messages for debugging
		debug=False;
		# limit of words to vocalize
		self.limit=1000
		# lexical analyzer
		self.analyzer=aranalex.analex.analex()
		# syntaxic analyzer
		self.anasynt=aranasyn.anasyn.SyntaxAnalyzer();
		#set the lexical analzer debugging
		self.analyzer.set_debug(debug);
		#set the lexical analzer  wrd limit
		self.analyzer.set_limit(self.limit);
		# Create the word freq dictionary
		wordfreq_DICTIONARY_INDEX={
		u'id':0, # id
		u'vocalized':1, # the vocalized word
		u'unvocalized':2,# the unvocalized form
		u'word_type':3, # the word frequency
		u'freq':4, # the word class
		u'future_type':5, # the haraka for verb			
		#u'trans':5,  # translation
		}
		#from   dictionaries.verb_dictionary  import *
		#self.wordfreq= wordfreqdictionaryclass.wordfreqDictionary('wordfreq', wordfreq_DICTIONARY_INDEX);

	def set_limit(self, limit):
		"""
		set the limit length of words to vocalize
		"""
		self.limit=limit;
		#set the lexical analzer  wrd limit
		self.analyzer.set_limit(self.limit);
	def tashkeel(self,text,suggestion=False, format='text'):
		"""
		Vocalize the text and give suggestion to improve tashkeel by user.
		@param text: input text.
		@type text: unicode.
		@return: vocalized text.
		rtype: dict of dict or text.
		"""
		# The statistical tashkeel must return a text.
		#comment this after tests
		text=self.statTashkeel(text);
		# test statistical tashkeel
		#return self.statTashkeel(text);
		
		detailled_syntax=self.fullStemmer(text);
		vocalized_text=u"";
		previous=None;
		list_dict=[];
		for word_analyze_list in detailled_syntax:
			# create a suggest list
			suggest=[];
			for item in word_analyze_list:
				voc=self.get_vocalized_word(item);
				suggest.append(voc);
			suggest.sort();
			word_analyze_list=self.anasynt.exclode_cases(word_analyze_list)		
			word_stemming_dict=self.choose_tashkeel(word_analyze_list,previous);
			# o ajust relation between words
			# if the actual word is transparent don't change the previous
			# add this to Sytaxic Analyser
			if not self.anasynt.is_transparent(word_stemming_dict):
				previous=word_stemming_dict;			
			#previous=word_stemming_dict;
			word=self.get_vocalized_word(word_stemming_dict);
			vocalized_text=u" ".join([vocalized_text,self.display(word,format)]);
			list_dict.append({'chosen':word,'suggest':u";".join(suggest)});
		if suggestion:
			return list_dict;
		else:
			return vocalized_text;

	def tashkeelOuputHtmlSuggest(self,text):
		"""
		Vocalize the text and give suggestion to improve tashkeel by user.
		@param text: input text.
		@type text: unicode.
		@return: vocalized text.
		rtype: dict of dict.
		"""
		return self.tashkeel(text,suggestion=True, format="html");
		
		
	def tashkeelOutputText(self,text):
		"""
		Vocalize the text witthout suggestion
		@param text: input text.
		@type text: unicode.
		@return: vocalized text.
		rtype: text.
		"""
		return self.tashkeel(text,suggestion=False, format="text");

		
	def choose_tashkeel(self, word_analyze_list,previous_stemming_dict=None):
		"""
		Choose a tashkeel for the current word, according to the previous one.
		@param : list of steming result of the word.
		@type word_analyze_list: list of dict;
		@param : the choosen previous word stemming.
		@type previous_stemming_dict:
		@return: the choosen stemming of the current word.
		@rtype:dict.
		"""
		word_analyze_list=self.anasynt.is_related(previous_stemming_dict,word_analyze_list);
		# select the first chosen tashkeel
		if len(word_analyze_list)>0:
			chosen= word_analyze_list[0];
		# select according to  word frequency
		freq=0;
		chosenFreq=0;
		#first chose the frequncy
		for item_dict in word_analyze_list:
			freq=self.getFrequency(item_dict);
			if freq>=chosenFreq:
				chosenFreq=freq;
		newList=[];
		for item_dict in word_analyze_list:
			freq=self.getFrequency(item_dict);
			if freq==chosenFreq:
				newList.append(item_dict);
		#print chosenFreq;
		#print len(word_analyze_list), len(newList);
		word_analyze_list=newList;
		#todo select the evident case if exists.
		forced=False;
		for item_dict in word_analyze_list:
			if not forced and self.forced_case(item_dict):
				chosen=item_dict;
				forced=True;
			if not forced and self.anasynt.is_noun(item_dict)and self.anasynt.is_mansoub(item_dict):
				chosen=item_dict;
			if not forced and self.anasynt.is_verb(item_dict):
				if self.anasynt.is_present(item_dict)  and not self.anasynt.is_passive(item_dict) and not (self.anasynt.is_mansoub(item_dict) or self.anasynt.is_majzoum(item_dict)) :
					chosen=item_dict;
				if self.anasynt.is_past(item_dict) and not self.anasynt.is_passive(item_dict):
					chosen=item_dict;
		return chosen;


	def get_vocalized_word(self,stemming_dict):
		"""
		Get the vocalized word from the stemming dict 
		@param stemming dict: dict of tags .
		@type stemming dicttext: dict.
		@return: vocalized word.
		rtype: unicode.
		"""	
		if stemming_dict.has_key('vocalized') and stemming_dict['vocalized']!=u"":
			chosen= stemming_dict['vocalized'];
		elif stemming_dict.has_key('word'):
			chosen= stemming_dict['word'];
		else: chosen="";
		return re.sub('-','',chosen);



	def fullStemmer(self, text):
		"""
		Do the lexical and syntaxic analysis of the text.
		@param text: input text.
		@type text: unicode.
		@return: syntaxic and lexical tags.
		rtype: dict of list of dict.
		"""
		result=[];
		result=self.analyzer.check_text(text);
		result=self.anasynt.context_analyze(result);
		return result;


	def forced_case(self,stemming_dict):
		"""
		if this case is forced according to the previous word.
		@param stemming_dict: dict of stemming.
		@type stemming_dict:
		@return: if forced.
		@rtype: True/False.
		"""
		if stemming_dict.has_key('tags'):
			if stemming_dict['tags'].find('*')>0:
				return True;
			else:
				return False;
		return False;

	def getFrequency(self,stemming_dict):
		"""
		Get the original word frequency
		@param stemming_dict: dict of stemming.
		@type stemming_dict:
		@return: frequency.
		@rtype: intger.
		"""
		# Done: Add frequency field to the stemming dict
		freq=stemming_dict.get('freq',0);
		# Temporary 
		# deprecated
		# original=stemming_dict.get('original','');
		# freq=self.wordfreq.getFreq(original);
		return freq;
	# generate a word stem according to the suffix


	def display(self, word, format="text"):
		"""
		format the vocalized word to be displayed on web interface.
		@param word: input vocalized word.
		@type word: unicode.
		@return: html code.
		rtype: unicode.
		"""
		format=format.lower();
		if format=="html":
			return u"<span id='vocalized' class='vocalized'>%s</span>"%word;
		elif format=='text':
			return word;
		else:
			return word;

	def assistanttashkeel(self,text):
		"""
		Vocalize the text.
		@param text: input text.
		@type text: unicode.
		@return: vocalized text.
		rtype: unicode.
		"""	
		detailled_syntax=self.fullStemmer(text);
		vocalized_text=u"";
		previous=None;
		for word_analyze_list in detailled_syntax:
			#word_analyze_list=self.anasynt.exclode_cases(word_analyze_list)		
			#word_stemming_dict=self.choose_tashkeel(word_analyze_list,previous);
			# o ajust relation between words 
			#previous=word_stemming_dict;
			for item in word_analyze_list:
				voc=self.get_vocalized_word(item);
				vocalized_text=u";".join([vocalized_text,voc]);
		return vocalized_text;





	def statTashkeel(self,text):
		"""
		Vocalize the text by statistical method according to the collocation dictionary
		@param text: input text.
		@type text: unicode.
		@return: statisticlly vocalized text.
		rtype: unicode.
		"""
		# get the word list
		wordlist=self.analyzer.tokenize(text);
		collo=collocations.CollocationClass();
		vocalized_text=u"";
		previous=u"";
		list_dict=[]; # returned resultat
		# temporarly used
		suggest=[];
		liste=wordlist;
		# use a list as a stack,
		# give two element from the end.
		# test the tow elements if they are collocated,
		# if collocated return the vocalized text
		# if else, delete the last element, and return the other to the list.
		newlist=collo.lookup(wordlist);
		#todo: return a text from the statistical tashkeel
		text=u" ".join(newlist);
		return text;
		# is done temporaly to test statistical tashkeel
		#for word in newlist:
		#	vocalized_text=u" ".join([vocalized_text,self.display(word)]);
		#	list_dict.append({'chosen':word,'suggest':u";".join(suggest)});
		#return list_dict;
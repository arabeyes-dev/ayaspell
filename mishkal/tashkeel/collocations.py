﻿#!/usr/bin/python
# -*- coding=utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        collocations
# Purpose:     Arabic automatic vocalization.
#
# Author:      Taha Zerrouki (taha.zerrouki[at]gmail.com)
#
# Created:     31-10-2011
# Copyright:   (c) Taha Zerrouki 2011
# Licence:     GPL
#-------------------------------------------------------------------------------
import collocationdictionary
#import collocationdictionary as colloDict
import collocation_const
import pyarabic.araby as araby
class CollocationClass:
	"""
        Arabic Collocations Class
	"""

	def __init__(self):
		self.min=2;
		self.max=5;
		Collocation_DICTIONARY_INDEX={
	u'id':0,
	u'vocalized':1,
	u'unvocalized':2,
	u'rule':3,
	u'category':4,
	u'note':5,
		}
		#from   dictionaries.verb_dictionary  import *
		self.colloDict=collocationdictionary.collocationDictionary('collocations', Collocation_DICTIONARY_INDEX);
		self.colloCache={};
		pass;
	def setMin(self,min0):
		# to avoid errers, verify the min
		self.min=min(min0,self.min);
		self.max=max(min0,self.max);		
	def setMax(self,max0):
		# to avoid errers, verify the max
		self.min=min(max0,self.min);
		self.max=max(max0,self.max);	
	def isCollocated(self,wordlist):
		"""
		Return The vocalized text if the word list is collocated.
		@param wordlist: word of list, 2 or more words.
		@type wordlist: list of unicode.
		@return : The collocation as a key if exists. else False.
		@rtype: dict/None.
		"""
		# The fisrt case is the two words collocation
		if len(wordlist)<2:
			return False;
		else:
			# get two element from the list start
			key=u' '.join(wordlist); 
			idlist=self.colloDict.lookup(key);
			# if the wordlist as key existes in collocation data base, 
			# insert its vocalization in a collocation cache dict
			if len(idlist)>=1 :
				if not self.colloCache.has_key(key): 
					id=idlist[0];
					#print idlist;
					self.colloCache[key]= self.colloDict.getAttribById(id,'vocalized')
				return key;
			else:
				return False;		
		return False;
	def ngramfinder(self,min,liste):
		"""
		Lookup for ngram (min number of words), in the word list.
		return a list of single words and collocations.
		@param wordlist: word of list, 2 or more words.
		@type wordlist: list of unicode.
		@param min: minimum number of words in the collocation
		@type min: integer.		
		@return : list of words and collocations, else False.
		@rtype: list /None.
		"""	
		newlist=[];
		while len(liste)>=min:
			sublist=[];
			for i in range(min):
				sublist.insert(0,liste.pop());
			#print "bsublis", sublist
			result=self.isCollocated(sublist);
			if result:
				newlist.append(result);
			else:
				newlist.append(sublist.pop());
				liste.extend(sublist);
		# rest element
		liste.reverse()
		newlist.extend(liste);
		newlist.reverse();
		return newlist


	def isPossibleCollocation(self,list2,context="",lenght=2):
		"""
		Guess if the given list is a possible collocation
		This is used to collect unkown collocations, from user input
		return True oor false
		@param wordlist: word of list, 2 or more words.
		@type wordlist: list of unicode.
		@param lenght: minimum number of words in the collocation
		@type lenght: integer.		
		@return : the rule of found collocation, 100 default.
		@rtype: interger.
		"""		
		if len(list2)<lenght:
			return 0;
		else:
			itemV1=list2[0];
			itemV2=list2[1];
			item1=araby.stripTashkeel(itemV1);
			item2=araby.stripTashkeel(itemV2);		
			#if item1[-1:] in (u".",u"?",u",",u'[', u']',u'(',')'):
			#	return 0;
			if  not collocation_const.token_pat.search(item1) or not collocation_const.token_pat.search(item2) :
				return -1;
			#else: return 100;
			elif item1 in collocation_const.ADDITIONAL_WORDS :
				return 10;
			elif item1 in collocation_const.NAMED_PRIOR :
				return 15;			
			elif (item2 not in collocation_const.SPECIAL_DEFINED):
				if  item2.startswith(u'ال') and  item1.startswith(u'ال'):#re.search(ur'^(ال|بال|وبال|فال|وال|لل|كال|فكال|ولل|فلل|فبال)', item1):
					return 20;
				elif item1.endswith(u'ة') and item2.startswith(u'ال'):
					return 30;

				#حالة الكلمات التي تبدأ بلام الجر والتعريف 
				# لا داعي لها لأنها دائما مجرورة
				#if  item2.startswith(u'لل'):
				#	return 40;
				elif item1.endswith(u'ة') and item2.endswith(u'ة')  :
					return 40;
				#if item1.endswith(u'ي') and item2.endswith(u'ي'):
				#	return 60;

				elif  context!=u"" and context in collocation_const.tab_noun_context and item2.startswith(u'ال') :
					return 50;
				#return True;

				elif item1.endswith(u'ات') and item2.startswith(u'ال') :
					return 60;
			return 100;


	def lookup(self,wordlist):
		"""
		Lookup for all ngrams , in the word list.
		return a list of vocalized words collocations.
		@param wordlist: word of list, 2 or more words.
		@type wordlist: list of unicode.
		@return : dict of words attributes like dict {'vocalized':vocalizedword list,'category': categoryOfCollocation}. else False.
		@rtype: dict of dict /None.
		"""
		#lookup for collocation from max number to min number of words in the collocation
		i=self.max;
		collolist=wordlist;
		while i>=self.min: 
			collolist= self.ngramfinder(i,collolist);
			i-=1;
		#Get the list of single words and collocations
		#collolist=self.ngrams(self.min, self.max, wordlist);
		##collodict={};
		newlist=[]
		#print repr(self.colloCache);
		for item in collolist:
			if self.colloCache.has_key(item):
				vocalized=self.colloCache[item];
				#lookup for collocations in dictionary
				# the dictionary conatins the vocalized collocation,
				# but it contains also a list of collocations non vocalized yet,
				# this 'vocalized value is empty, then we use this feature to collect the user feed back and correction
				if vocalized!=u"":
					# if the vocalization is vocalized, we don't mention it
					newlist.append(vocalized);
				else:
					# ig the collocation isn't vocalized, we delemeit it by ~, to collect corrections.
					newlist.append(u"~"+item+u"~");				
			
			else:
				newlist.append(item);
		return newlist;

		
#Class test
if __name__ == '__main__':

	collo=CollocationClass()
	wordlist=[u"قبل",u"صلاة",u"الفجر",u"كرة",u"القدم",u"في",u"دولة",u"قطر"]
	newlist=collo.lookup(wordlist);
	for word in newlist:
		print word.encode('utf8');
	text=u" ".join(newlist);
	print text.encode('utf8');

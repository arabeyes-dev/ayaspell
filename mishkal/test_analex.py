﻿from analex import *


# MAIN
# read data from a file
#mode='verb'
mode='noun'
#mode='all'
if mode=='verb':
	filename="samples/quran_verb.txt"
elif mode=='noun':
	#filename="samples/quran_noun.txt"
	filename="samples/sample_noun.txt"	
else :
	filename="samples/quran_noun.txt";
try:
	myfile=open(filename)
	text=(myfile.read()).decode('utf8');

	if text==None:
		text=u""
except:
	text=u"أسلم"

debug=False;
limit=500
analyzer=analex()
analyzer.set_debug(debug);
analyzer.set_limit(limit);
if mode=='verb':
	result=analyzer.check_text_as_verbs(text);
elif mode=='noun':
	result=analyzer.check_text_as_nouns(text);
else:
	result=analyzer.check_text(text);
print '----------------Global result-------'
print result;
for i in range(len(result)):
#		print "--------تحليل كلمة  ------------", word.encode('utf8');
	for analyzed in  result[i]:
		for key in analyzed.keys():
			print (u"\t"+analyzed[key]).encode('utf8'),
		print;
	print;
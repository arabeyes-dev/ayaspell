﻿$().ready(function() {
  $('#btn1').click( function() {
      $.getJSON(script+"/ajaxGet", {}, function(d){
        $("#rnd").text(d.rnd);
        $("#result").text(d.result);		
        $("#t").text(d.time);
      });
  }); 

  $('#stripharakat').click( function() {
	//	$("#result").html("<pre>TATAH\nNTATAH</pre>");
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"StripHarakat"}, function(d){
      $("#result").html("<pre>"+d.result+"</pre>");
        //"#result").text(d.time);
      }); 
	        }); 
	  $('#csv2data').click( function() {
      $.getJSON(script+")s/ajaxGet", {text:document.NewForm.InputText.value,action:"CsvToData"}, function(d){
        $("#result").html("<pre>"+d.result+"</pre>");
        //"#result").text(d.time);
      });
  }); 
  $('#number').click( function() {
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"NumberToLetters"}, function(d){
        $("#result").html("<pre>"+d.result+"</pre>");
        //"#result").text(d.time);
      });
  });  
  
 //Unshape text 
    $('#unshape').click( function() {
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Unshape"}, function(d){
        $("#result").html("<pre>"+d.result+"</pre>");
      });
	  });
 //move result into input 
    $('#move').click( function() {
       document.NewForm.InputText.value=$("#result").text();
  });
   $('#stem').click( function() {
		$("#loading").slideDown();
        var $table = $('<table/>');
        var table = $table.attr( "border", "1" )[0];
        var headers = ["<tr>",
				"<th>المدخل</th>", "<th>تشكيل</th>","<th>الأصل</th>","<th>السابقة</th>", "<th>الجذع</th>",
				"<th>اللاحقة</th>", "<th>الحالة الإعرابية</th>","<th>الجذر</th>", "<th>النوع</th>",
				"</tr>"
				].join('');
         $table.append( headers ); 
		var item="";
		$("#result").html( "" );
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"LightStemmer"}, function(d){
		for (k in d.result)
		{
		var tbody = document.createElement('tbody');
		if (d.result[k].length==0)
		{
		var tr = document.createElement('tr');
		var td = document.createElement('td');
		td.appendChild(document.createTextNode( k) );
		tr.appendChild(td);
		for (j=0;j<7;j++)
		{
		var td = document.createElement('td');
		td.appendChild(document.createTextNode("-") );
		tr.appendChild(td);
		}
		tbody.appendChild(tr);
		}
		else
		{
	  for (i=0;i<d.result[k].length;i++)
		{
		var tr = document.createElement('tr');
		item=d.result[k][i];
		var td = document.createElement('td');
		td.appendChild(document.createTextNode( item['word']) );
		tr.appendChild(td);

		td = document.createElement('td');
		td.appendChild(document.createTextNode( item['vocalized']) );
		tr.appendChild(td);		
		td = document.createElement('td');
		td.appendChild(document.createTextNode( item['original']) );
		tr.appendChild(td);	
		td = document.createElement('td');
		td.appendChild(document.createTextNode(item['procletic']+'-'+item['prefix']) );
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode( item['stem']) );
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode( item['suffix']+'-'+item['encletic']) );
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode( item['tags']) );
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode( item['root']) );
		tr.appendChild(td);
		td = document.createElement('td');
		td.appendChild(document.createTextNode( item['type']) );
		tr.appendChild(td);	
		tbody.appendChild(tr);
		}
		}
		table.appendChild(tbody);
		}
		$("#result").append( $table );		
      });
	  $("#loading").slideUp();
  }); 
  
  
   $('#tokenize').click( function() {
		var item;
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Tokenize"}, function(d){
		$("#result").html( "" );
	  for (i=0;i<d.result.length;i++)
		{
		$("#result").append( d.result[i]+"<br/>" );
		}
      });
  }); 
  // inverse order
   $('#inverse').click( function() {
		var item;
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Inverse"}, function(d){
		$("#result").html( "" );
	  for (i=0;i<d.result.length;i++)
		{
		$("#result").append( d.result[i]+"<br/>" );
		}
      });
  });   
  
// Ajust an Arabic poetry in two columns  
   $('#poetry').click( function() {
        var $table = $('<table/>');
        var table = $table.attr( "border", "0" )[0];
		$table.attr( "width",'600');
		//$table.attr( "style",'text-align: justify; text-justify: newspaper; text-kashida-space: 100;”);
        var headers = ["<tr>",
				"<th>الصدر</th>", "<th>العجز</th>",
				"</tr>"
				].join('');
         $table.append( headers ); 
		var item;
		$("#result").html( "" );		
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Poetry"}, function(d){
       for (i=0;i<d.result.length;i++)
		{
		var tr = document.createElement('tr');	
		item=d.result[i];
		var td = document.createElement('td');
		td.appendChild(document.createTextNode( item[0]) );
		tr.appendChild(td);	
		td = document.createElement('td');
		td.appendChild(document.createTextNode( item[1]) );
		tr.appendChild(td);
		table.appendChild(tr);
		}

		$("#result").append( $table );		

      });
  });   
 
  $('#romanize').click( function() {
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Romanize"}, function(d){
        $("#result").html("<pre>"+d.result+"</pre>");

      });
  });
  
  // normalize text
  $('#normalize').click( function() {
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Normalize"}, function(d){
        $("#result").html(d.result);

      });
  });    
    $('#wordtag').click( function() {
        var $table = $('<table/>');
        var $div = $('<div/>');		
       var div = $div[0];		
        var table = $table.attr( "border", "0" )[0];
		$table.attr( "width",'600');
		//$table.attr( "style",'text-align: justify; text-justify: newspaper; text-kashida-space: 100;”);
        var headers = ["<tr>",
				"<th>الكلمة</th>", "<th>تصنيفها</th>",
				"</tr>"
				].join('');
		
         $table.append( headers ); 
		$("#result").html( "" );
		var item;
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Wordtag"}, function(d){
		$("#result").html("" );
	  for (i=0;i<d.result.length;i++)
		{
			
		item=d.result[i];
		var span = document.createElement('span');
		//span.appendChild(document.createTextNode(  item.word) );
		//div.appendChild(span);	
		//span = document.createElement('span');
		span.setAttribute('class',item.tag);
		span.appendChild(document.createTextNode(" "+item.word) );
		div.appendChild(span);


		//display as table
		var tr = document.createElement('tr');
		var td = document.createElement('td');		
		td.appendChild(document.createTextNode(  item.word) );
		tr.appendChild(td);	
		td = document.createElement('td');
		td.setAttribute('class',item.tag);
		td.appendChild(document.createTextNode(item.tag) );
		tr.appendChild(td);
		table.appendChild(tr);
//		$("#result").append(+"  "++"<br/>" );
		}
		$("#result").append($div );
		$("#result").append($table );
		});
		

  });
  
  
      $('#language').click( function() {
        var $div = $('<div/>');		
       var div = $div[0];		
		$("#result").html( "" );
		var item;
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Language"}, function(d){
		$("#result").html("" );
	  for (i=0;i<d.result.length;i++)
		{
			
		item=d.result[i];
		var span = document.createElement('span');
		span.setAttribute('class',item[0]);
		span.appendChild(document.createTextNode(item[1]) );
		div.appendChild(span);
		}
		$("#result").append($div );
		});
		

  });
  
        $('#latexlanguage').click( function() {
        var $div = $('<pre/>');		
       var div = $div[0];
	   var txt="";
		$("#result").html( "" );
		var item;
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Language"}, function(d){
		$("#result").html("" );
	  for (i=0;i<d.result.length;i++)
		{
			
		item=d.result[i];
		var span = document.createElement('span');
		span.setAttribute('class',item[0]);
		if (item[0]=='arabic') txt="\\AR{"+item[1]+"}";
		else txt=item[1];
		
		span.appendChild(document.createTextNode(txt) );
		div.appendChild(span);
		}
		$("#result").append($div );
		});
		

  });
  
  //Latex some features
  $('#itemize').click( function() {
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Itemize"}, function(d){
        $("#result").html("<pre>"+d.result+"</pre>");

      });
 });
 
  $('#tabulize').click( function() {
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Tabulize"}, function(d){
        $("#result").html("<pre>"+d.result+"</pre>");

      });
  }); 

  $('#tabbing').click( function() {
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Tabbing"}, function(d){
        $("#result").html("<pre>"+d.result+"</pre>");

      });
  }); 
  
  
// generate all affixation form of a word  
    $('#affixate').click( function() {
        var $table = $('<table/>');
        var table = $table.attr( "border", "0" )[0];
		$table.attr( "width",'600');
		//$table.attr( "style",'text-align: justify; text-justify: newspaper; text-kashida-space: 100;”);
        var headers = ["<tr>",
				"<th>الكلمة</th>", "<th>تقطيعها</th>",
				"</tr>"
				].join('');
         $table.append( headers ); 
		 		$("#result").html( "" );
		var item;
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Affixate"}, function(d){
		$("#result").html("" );
	  for (i=0;i<d.result.length;i++)
		{
		var tr = document.createElement('tr');	
		item=d.result[i];
		var td = document.createElement('td');
		td.appendChild(document.createTextNode(  item.standard) );
		tr.appendChild(td);	
		td = document.createElement('td');
		td.appendChild(document.createTextNode( item.affixed) );
		tr.appendChild(td);
		table.appendChild(tr);
		
//		$("#result").append(+"  "++"<br/>" );
		}
		$("#result").append($table );
		});
  });  
  $('#btn2').click( function() {
      $.getJSON(script+"/ajaxToUpper", {text:document.myForm.typedTxt.value},function(d){
        $("#u").text(d);
      });

      $.getJSON(script+"/ajaxSplit", {by:'-',text:document.myForm.typedTxt.value},function(d){
        var ul=$("#s");
        ul.html('');
        for (var i in d){
          var li=$("<li/>").html(d[i]).appendTo(ul);
        }

      });

    });
   $('#tashkeel').click( function() {
      $.getJSON(script+"/ajaxGet", {text:document.NewForm.InputText.value,action:"Tashkeel"}, function(d){
      $("#result").html("<p class=\'tashkeel\'>"+d.result+"</p>");
      }); 

	        }); 
   $('.vocalized').live("click", function() {
     //$("#vocalized").slideDown("slow");
	 var myword=$(this);
      $.getJSON(script+"/ajaxGet", {text:myword.html(),action:"AssistantTashkeel"}, function(d){
	 var list=d.result.split(';');
	var p = myword;
	var position = p.position();
	//text+="left: " +  + ", top: " + ;
	 //var text="<select style='position:fixed;top:"+Math.ceil(position.top)+"px; left:"+Math.ceil(position.left)+"px;'>";
	var text="<select class='txkl'>";	 
	 for ( i in list)
		{
		if (list[i]!="") text+="<option>"+list[i]+"</option>";
		}
		text+="</select>";

		
	 //$("#resultat").html(text);
	 $(".vocalized").replaceWith(text);	 
      });
	
	 });
	 //
   $('.voc,.voc2').live("mouseover", function() {
   $('.voc').toggleClass('vocalized');
});   
   $('.txkl').live('change',function() {
  	var text=$(".txkl").val();
	text="<span class='vocalized'>"+text+"</span>";
	 $(".txkl").replaceWith(text);	 
	 });

});
//});

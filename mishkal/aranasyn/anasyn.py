﻿#!/usr/bin/python
# -*- coding=utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        syn_const
# Purpose:     Arabic syntaxic analyser.
#
# Author:      Taha Zerrouki (taha.zerrouki[at]gmail.com)
#
# Created:     31-10-2011
# Copyright:   (c) Taha Zerrouki 2011
# Licence:     GPL
#-------------------------------------------------------------------------------


import   syn_const
import pyarabic.araby as araby
class SyntaxAnalyzer:
	"""
        Arabic Syntax analyzer
	"""

	def __init__(self):
		pass;

	def context_analyze(self,detailed_stemming_dict):
		"""
		Syntaxic analysis of stemming results.
		@param detailed_stemming_dict: detailed stemming dict.
		@type detailed_stemming_dict:list of dict of dict;
		@return: detailed syntaxic result with syntaxic tags.
		@rtype: list of dict of dict;
		"""
		previous=u"";
		ignore=False;
		for word_result in detailed_stemming_dict:
			for stemming_dict in word_result:
				# Todo treat the actual word
				if self.is_noun(stemming_dict):
					if self.is_direct_jar(previous) and  self.is_majrour(stemming_dict):
						stemming_dict['tags']+=u'*';
					if self.is_direct_naseb(previous) and  self.is_mansoub(stemming_dict):
						stemming_dict['tags']+=u'*';						
					if self.is_initial(previous) and  self.is_marfou3(stemming_dict):
						stemming_dict['tags']+=u'*';						
					if self.is_direct_nominal_factor(previous) and not self.has_procletic(stemming_dict):
						stemming_dict['tags']+=u'#';						
					if self.is_direct_rafe3(previous) and self.is_marfou3(stemming_dict):
						stemming_dict['tags']+=u'*';
				#verb
				elif self.is_verb(stemming_dict):
					if self.is_direct_jazem(previous)  and  self.is_majzoum(stemming_dict):
						stemming_dict['tags']+=u'*';
					if self.is_direct_verb_naseb(previous) and  self.is_mansoub(stemming_dict):
						stemming_dict['tags']+=u'*';
					if self.is_direct_verbal_factor(previous) and not self.has_procletic(stemming_dict):
						stemming_dict['tags']+=u'#';
					if self.is_direct_verb_rafe3(previous) and self.is_marfou3(stemming_dict):
						stemming_dict['tags']+=u'*';
				elif self.is_pounct(stemming_dict):
					#if the word is pounctuation and it's transparent, the effect of previous factor will be kept
					#then we ignore this word in the next step, 
					#the variable 'previous' will not take the actual word.
					if self.is_transparent(stemming_dict):
						ignore=True;
				# For the transparent stop words like هذا وذلك من اسماء الإشارة
				if self.is_transparent(stemming_dict):
					ignore=True;
				#print  word_result;
			# if the actual word is not ignorable; change the previous
			# else keep the 'previous' variable value. and change the 'ignore' value.
			if not ignore:
				previous=word_result[0]['word'];
				ignore=False;
			else:
				ignore=False;
		detailed_syn_result=detailed_stemming_dict;
		return detailed_syn_result;

	def is_direct_jar(self, word):
		"""
		Return True if the word is a direct Jar.
		@param word: input word.
		@type word: unicode;
		@return: direct Jar.
		@rtype: True/False;
		"""	
		if araby.stripTashkeel(word) in syn_const.JAR_LIST:
			return True;
		return False;

	def is_direct_verb_naseb(self, word):
		"""
		Return True if the word is a direct Naseb of verb.
		@param word: input word.
		@type word: unicode;
		@return: direct Naseb.
		@rtype: True/False;
		"""		
		if word in syn_const.VERB_NASEB_LIST:
			return True;
		return False;


	def is_direct_jazem(self, word):
		"""
		Return True if the word is a direct Jazem.
		@param word: input word.
		@type word: unicode;
		@return: direct Jazem.
		@rtype: True/False;
		"""		
		if word in syn_const.JAZEM_LIST:
			return True;
		return False;


	def is_direct_naseb(self, word):
		"""
		Return True if the word is a direct Naseb of noun.
		@param word: input word.
		@type word: unicode;
		@return: direct Naseb of noun.
		@rtype: True/False;
		"""		
		if word in syn_const.NOUN_NASEB_LIST:
			return True;
		return False;


	def is_initial(self, word):
		"""
		Return True if the word mark the begin of next sentence.
		@param word: input word.
		@type word: unicode;
		@return: direct initial.
		@rtype: True/False;
		"""		
		if word==u"":
			return True;
		elif word in (u'.',u'?', u';', u':'):
			return True;
		return False;		


	def is_direct_rafe3(self, word):
		"""
		Return True if the word is a direct Rafe3.
		@param word: input word.
		@type word: unicode;
		@return: direct Rafe3.
		@rtype: True/False;
		"""		
		if word in syn_const.RAFE3_LIST:
			return True;
		return False;


	def is_direct_verb_rafe3(self, word):
		"""
		Return True if the word is a direct Rafe3 of verb
		@param word: input word.
		@type word: unicode;
		@return: direct Rafe3 of verb.
		@rtype: True/False;
		"""		
		if word in syn_const.VERB_RAFE3_LIST:
			return True;
		return False;		


	def is_noun(self, stemming_dict):
		"""
		Return True if the word is a noun.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: is a noun.
		@rtype: True/False;
		"""			
		if stemming_dict.has_key('type') and stemming_dict['type'].find(u'Noun')>=0:#stemming_dict['type']=='Noun':
			return True;
		else:
			return False;


	def is_verb(self, stemming_dict):
		"""
		Return True if the word is a verb.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: is a verb.
		@rtype: True/False;
		"""			
		if stemming_dict.has_key('type') and stemming_dict['type'].find(u'Verb')>=0:#stemming_dict['type']=='Verb':
			return True;
		else:
			return False;			

	def is_pounct(self, stemming_dict):
		"""
		Return True if the word is a pounctuation.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: is a verb.
		@rtype: True/False;
		"""			
		if stemming_dict.has_key('type') and stemming_dict['type'].find(u'POUNCT')>=0:
			return True;
		else:
			return False;


	def is_transparent(self, stemming_dict):
		"""
		Return True if the word has the state transparent, which can trasnpose the effect of the previous factor.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the state transparent.
		@rtype: True/False;
		"""
		#temporary, 
		# the transparent word are stopwords like هذا وذلك
		# the stopword tags have اسم إشارة,
		# a pounctuation can has the transparent tag like quotes., which havent any gramatical effect.
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'شفاف')>=0:
			return True;
		elif stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'إشارة')>=0:
			return True;
		else:
			return False;


	def is_majrour(self, stemming_dict):
		"""
		Return True if the word has the state majrour.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the state majrour.
		@rtype: True/False;
		"""				
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'مجرور')>=0:
			return True;
		else:
			return False;


	def is_majzoum(self, stemming_dict):
		"""
		Return True if the word has the state majrour.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the state majrour.
		@rtype: True/False;
		"""		
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'مجزوم')>=0:
			return True;
		else:
			return False;


	def is_mansoub(self, stemming_dict):
		"""
		Return True if the word has the state mansoub.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the state mansoub.
		@rtype: True/False;
		"""			
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'منصوب')>=0:
			return True;
		else:
			return False;





	def is_marfou3(self, stemming_dict):
		"""
		Return True if the word has the state marfou3.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the state marfou3.
		@rtype: True/False;
		"""		
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'مرفوع')>=0:
			return True;
		else:
			return False;


	def is_defined(self, stemming_dict):
		"""
		Return True if the word has the state definde.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the state defined.
		@rtype: True/False;
		"""		
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'تعريف')>=0:
			return True;
		else:
			return False;


	def is_past(self, stemming_dict):
		"""
		Return True if the word has the tense past.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the  tense past.
		@rtype: True/False;
		"""		
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'ماضي')>=0:
			return True;
		else:
			return False;


	def is_passive(self, stemming_dict):
		"""
		Return True if the word has the tense passive.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the  tense passive.
		@rtype: True/False;
		"""	
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'مجهول')>=0:
			return True;
		else:
			return False;


	def is_present(self, stemming_dict):
		"""
		Return True if the word has the tense present.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the  tense present.
		@rtype: True/False;
		"""	
	
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'مضارع')>=0:
			return True;
		else:
			return False;


	def is_3rdperson(self, stemming_dict):
		"""
		Return True if the word has the 3rd person.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the 3rd persontense.
		@rtype: True/False;
		"""	
		if stemming_dict.has_key('tags') and ((stemming_dict['tags'].find(u':هي:')>=0 or stemming_dict['tags'].find(u':هو:')>=0) and not stemming_dict['tags'].find(u'مفعول به')>=0):
		#if stemming_dict.has_key('tags') and ((stemming_dict['tags'].find(u':هي:')>=0 or stemming_dict['tags'].find(u':هو:')>=0) and stemming_dict['tags'].find(u'مفعول به')>=0):
			return True;
		else:
			return False;			


	def is_added(self, stemming_dict):
		"""
		Return True if the word has the state added مضاف.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the state added.
		@rtype: True/False;
		"""		
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'مضاف')>=0:
			return True;
		else:
			return False;


	def is_tanwin(self, stemming_dict):
		"""
		Return True if the word has tanwin.
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has tanwin.
		@rtype: True/False;
		"""		
		if stemming_dict.has_key('tags') and stemming_dict['tags'].find(u'تنوين')>=0:
			return True;
		else:
			return False;			


	def is_direct_nominal_factor(self, word):
		"""
		Return True if the word is a direct nominal factor.
		@param word: actual word.
		@type word: unicode;
		@return:  is a direct nominal factor.
		@rtype: True/False;
		"""		
		if word in syn_const.NOMINAl_FACTOR_LIST:
			return True;
		return False;		


	def is_direct_verbal_factor(self, word):
		"""
		Return True if the word is a direct verbal factor.
		@param word: actual word.
		@type word: unicode;
		@return:  is a direct verbal factor.
		@rtype: True/False;
		"""		
		if word in syn_const.VERBAL_FACTOR_LIST:
			return True;
		return False;
	#  حالة المضاف إليه


	def is_related(self,stemming_dict_previous, word_result):
		"""
		study the relation between the actual word is related to the previous.
		@param stemming_dict_previous: The steming dict of the previous word.
		@type stemming_dict_previous: dict;
		@param word_result: The steming dict of the previous word.
		@type word_result: list of dict;
		@return: the word result dictionary with related tags.
		@rtype: list of dict;
		"""		
	# المضاف والمضاف إليه
		if stemming_dict_previous and self.is_noun(stemming_dict_previous) and not self.is_defined(stemming_dict_previous) and  not self.is_added(stemming_dict_previous) and not self.is_tanwin(stemming_dict_previous) :
			for stemming_dict in word_result:
				# Todo treat the actual word
				if self.is_noun(stemming_dict) and self.is_majrour(stemming_dict) and (stemming_dict['procletic']==u"ال" or stemming_dict['procletic']==u""):
						stemming_dict['tags']+=u'*';
	# منعوت والنعت
		if stemming_dict_previous and self.is_noun(stemming_dict_previous) and self.is_defined(stemming_dict_previous):
			for stemming_dict in word_result:
				# Todo treat the actual word
				if self.is_noun(stemming_dict) and (stemming_dict['procletic']==u"ال" or stemming_dict['procletic']==u"وال"):
					if self.is_majrour(stemming_dict) and self.is_majrour(stemming_dict_previous):
						stemming_dict['tags']+=u'*';						
					if self.is_mansoub(stemming_dict) and self.is_mansoub(stemming_dict_previous):
						stemming_dict['tags']+=u'*';				
					if self.is_marfou3(stemming_dict)and self.is_marfou3(stemming_dict_previous):
						stemming_dict['tags']+=u'*';
	# الفعل والفاعل أو نائبه
		if stemming_dict_previous and self.is_verb(stemming_dict_previous) and self.is_3rdperson(stemming_dict_previous):
			for stemming_dict in word_result:
				# Todo treat the actual word
				if self.is_noun(stemming_dict) and (stemming_dict['procletic']==u"ال" or stemming_dict['procletic']==u""):
					if self.is_marfou3(stemming_dict):
						stemming_dict['tags']+=u'*';
	
		return word_result;

	def has_procletic(self, stemming_dict):
		"""
		Return True if the word has procletic
		@param stemming_dict: one stemming dict with lexical and syntaxial tags.
		@type stemming_dict: dict;
		@return: has the procletic.
		@rtype: True/False;
		"""	
		if stemming_dict.has_key('procletic') and stemming_dict['procletic']!=u"":
			return True;
		else:
			return False;	

	def exclode_cases(self, word_result):
		"""
		exclode imcompatible cases
		@param word_result: The steming dict of the previous word.
		@type word_result: list of dict;
		@return: the filtred word result dictionary with related tags.
		@rtype: list of dict;
		"""		
	# حالة تحديد نمط الكلمة من السياق
		new_word_result=[];
		for stemming_dict in word_result:
			if stemming_dict['tags'].find('#')>0:
				new_word_result.append(stemming_dict);
		if len(new_word_result)>0:
			return new_word_result;
		else:
			return word_result;
		return word_result;
﻿#!/usr/bin/python
# -*- coding=utf-8 -*-
#-------------------------------------------------------------------------------
# Name:        syn_const
# Purpose:     Arabic syntaxic analyser constants.
#
# Author:      Taha Zerrouki (taha.zerrouki[at]gmail.com)
#
# Created:     31-10-2011
# Copyright:   (c) Taha Zerrouki 2011
# Licence:     GPL
#-------------------------------------------------------------------------------

RAFE3_LIST=set([
u'أنه',
u'أنك',
u'أنها',
u'بأنها',
u'بأنه',
u'وأنها',
u'فأنها',
u'فأنه',
u'كأنه',
u'كأنها',

# yahia alhadj
u'كان',
u'يكون',
u'كانت',
u'صار',
u'صارت',
u'يصير',
u'أمسى',
u'ليس',
u'ليست',
u'ظلّ',
u'ظلّت',
u'أضحى',
u'أضحت',
u'يضحي',
u'أصبح',
u'أصبحت',
u'يصبح',
u'بات',
u'باتت',
u'يبيت',
u'مازال',
u'لازال',
u'لايزال',
u'لازالت',
u'مايزال',
u'مازالت',
u'ماتزال',
u'مابرح',
u'مايبرح',
u'مابرحت',
u'ماانفك',
u'ماانفكّت',
u'ماينفك',
u'لاينفك',
u'مادام',
u'مادامت',
u'نعم',
u'بئس',
u'حبذا',


u'هل',
#u'من',
u'ما',
u'متى',
u'أين',
u'ماذا',
u'كيف',
u'أيان',
u'هو',
u'هي',
]);
JAR_LIST=set([
u'من', 
u'عن', 
u'إلى',
u'على',
u'في',
u'رب',
u'منذ',
u'مذ',
u'عدا',
u'خلا',
u'حاشا',

u'عند',
u'أمام',
u'وراء',
u'خلف',
u'مع',
u'قبل',
u'بعد',
u'تحت',
u'أي',
u'كلّ',
u'بعض',
u'غير',
u'سوى',
u'ليل',
u'شمال',
u'جنوب',
u'يمين',
u'شرق',
u'غرب',
u'شطر',
u'أسفل',
u'أعلى',
u'جنب',
u'جانب',
u'تلقاء',
u'قدام',
u'فوق',
u'أعلى',
u'شهر',
u'سنة',
u'غروب',
u'شروق',
u'دون',
u'شهور',

u'يوم',
u'حين',
u'ساعة',
u'زمان',
u'أزمان',
u'أيام',
u'أوقات',
u'وقت',
u'لحظة',

u'خلال',
u"بدون",

u"أثناء",
u"ذات",
u"ذو",
u"ذوو",
u"ذوات",
u"ذوي",
u"بن",
u"ابن",
u"بنت",
u'بين',
]);
JAZEM_LIST=set([
u'لم',
u'لما',
u'مهما',
u'حيثما',
u'أينما',
u'كيفما',
]);
VERB_NASEB_LIST=set([
u'أن',
u'كي',
u'لن',
u'حتى',
u'إذن',
]);
VERB_RAFE3_LIST=set([

u'هل',
u'من',
u'ما',
u'متى',
u'أين',
u'ماذا',
u'كيف',
u'أيان',


u'يوم',
u'حين',
u'ساعة',
u'زمان',
u'أزمان',
u'أيام',
u'أوقات',
u'وقت',
u'لحظة',

]);

NOUN_NASEB_LIST=set([
u'أن',
u'إن',
u'كأن',
u'لكن',
u'ليت',
u'لعلّ',
]);

VERBAL_FACTOR_LIST=set([
		u"قد",
		u"فقد",
		u"وقد",
		u"لن",
		u"لم",
]);

NOMINAl_FACTOR_LIST=set([
	u"في",
		u"بأن",
		u"بين",
		u"ففي",
		u"وفي",
		u"عن",
		u"إلى",
		u"على",
		u"بعض",
		u"تجاه",
		u"تلقاء",
		u"جميع",
		u"حسب",
		u"سبحان",
		u"سوى",
		u"شبه",
		u"غير",
		u"كل",
		u"لعمر",
		u"مثل",
		u"مع",
		u"معاذ",
		u"نحو",
		u"خلف",
		u"أمام",
		u"فوق",
		u"تحت",
		u"يمين",
		u"شمال",
		u"دون",
		u"من",
		u"بدون",
		u"خلال",
		u"أثناء",
]);



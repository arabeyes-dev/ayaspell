﻿#!/usr/bin/python
# -*- coding=utf-8 -*-

import sys
import re
import string
import datetime
import  getopt
import os
from ar_adv import *

scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
def usage():
# "Display usage options"
	print "(C) CopyLeft 2009, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-V | --version]\tprogram version"
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname
	print "\r\nThis program is licensed under the GPL License\n"

def grabargs():
#  "Grab command-line arguments"
	all = False;
	segmentation=False;
	dictionarybased=False;
	templatesearch=False;
	rootextraction=False;
	vocalized=False;
	convert=False;
	fname = ''

	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hVdrtsv:f:",
                               ["help", "version","dict", "root", "template","all",
                                "seg", "vocalized", "file="],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-V", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-s", "--seg"):
			segmentation = True
		if o in ("-f", "--file"):
			fname = val
		if o in ("-d", "--dict"):
			dictionarybased = True
		if o in ("-r", "--root"):
			rootextraction=True;
		if o in ("-t", "--template"):

			templatesearch =True;
		if o in ("-v", "--vocalized"):
			vocalized =True;
		if o in ("-c", "--convert"):
			convert =True;
	return (fname,segmentation,dictionarybased,templatesearch,rootextraction,vocalized,convert)

def main():

	filename,segmentation,dictionarybased,templatesearch,rootextraction,vocalized,convert=grabargs()
	print "file name ",filename
#ToDo1 ref
	if (not filename):
		usage()
		sys.exit(0)
	option="";
	try:
		fl=open(filename);
	except :
		print " Error :No such file or directory: %s" % filename
		return None;
	line=fl.readline().decode("utf8");
	text=u""
	limit=12000;
	counter=0;
	counter_match=0;
	counter_stop_words=0;
	counter_stop_words_match=0;
	counter_verb=0;
	counter_noun=0;
	while line and counter<limit:
##		text=" ".join([text,chomp(line)])
		line=fl.readline().decode("utf8");
	#print text.encode("utf8");
		listword=line.split("\t");
		if len(listword)>=2:
		  word=listword[0];
		  guessed_type=u""
		  type_word_arabic=listword[1];
		  if type_word_arabic.find(u"فعل")>=0:
		      type_word=u"v"
		  elif type_word_arabic.find(u"اسم")>=0 :
		      type_word=u"n"
		  else:
		      type_word=u"t"
		      counter_stop_words+=1;
		      if is_stopword(word):
		          counter_stop_words_match+=1;
		          guessed_type=u"t"
		      else:
		          guessed_type=u"p"
		          print word.encode("utf8");

##		      guessed_type=u""
		  if type_word in("n","v"):
		      counter+=1;
		      word=word.strip(u" ")

##		      guessed_type=u""
		      if is_possible_verb(word):guessed_type+=u"v";
		      if is_possible_noun(word):guessed_type+=u"n";
		      if type_word==guessed_type:
		          counter_match+=1;
		      if guessed_type in ("n","v"):
			    if guessed_type=="n":counter_noun+=1;
			    elif guessed_type=="v":counter_verb+=1;
		      if type_word not in guessed_type:
		          print word.encode("utf8"),len(word),u"\t",
		          print u"\t".join([guessed_type,type_word]),
		          print u"\t",type_word_arabic.encode("utf8"),
	print "--------------------",datetime.datetime.now(),"-------------------------"
	print "nb match word",counter_match,counter,round(counter_match*100.0/counter,2),"%"
	print "stop words ",counter_stop_words,"reprsent ",round(counter_stop_words*100.0/counter,2) ,"%"
	print "found words ",round(counter_match*100.0/(counter),2),"\%"
	print "found verbs ",counter_verb, round(counter_verb*100.0/counter_match,2),"\%"
	print "found noun ",counter_noun, round(counter_noun*100.0/counter_match,2),"\%"
	print "match stop word ",counter_stop_words_match

if __name__ == "__main__":
  main()








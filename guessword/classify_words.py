﻿#!/usr/bin/python
# -*- coding=utf-8 -*-

import sys,re,string,time
import sys, getopt, os
from ar_adv import *

scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
def usage():
# "Display usage options"
	print "(C) CopyLeft 2007, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-V | --version]\tprogram version"
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname

def grabargs():
#  "Grab command-line arguments"
	all = False;
	segmentation=False;
	dictionarybased=False;
	templatesearch=False;
	rootextraction=False;
	vocalized=False;
	convert=False;
	fname = ''

	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hVdrtsv:f:",
                               ["help", "version","dict", "root", "template","all",
                                "seg", "vocalized", "file="],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-V", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-s", "--seg"):
			segmentation = True
		if o in ("-f", "--file"):
			fname = val
		if o in ("-d", "--dict"):
			dictionarybased = True
		if o in ("-r", "--root"):
			rootextraction=True;
		if o in ("-t", "--template"):

			templatesearch =True;
		if o in ("-v", "--vocalized"):
			vocalized =True;
		if o in ("-c", "--convert"):
			convert =True;
	return (fname,segmentation,dictionarybased,templatesearch,rootextraction,vocalized,convert)

def main():

	filename,segmentation,dictionarybased,templatesearch,rootextraction,vocalized,convert=grabargs()
	print "file name ",filename
#ToDo1 ref
	if (not filename):
		usage()
		sys.exit(0)
	option="";
	text=readfile(filename);
	text=text_treat(text);
	listword=text.split(" ");
	previous_word="";
	counter=0;
	counter_match=0;
	counter_stop_words=0;
	counter_verb=0;
	counter_noun=0;
	for word in listword:
	    word=word.strip(" ")
	    print word.encode("utf8"),
	    print  "\t",
	    guess=u""
	    if is_stopword(word):
		    guess+=u"t";
		    counter_stop_words+=1;
	    else:
	       if is_possible_verb(word):
		      guess+= u"v";
	       if is_possible_noun(word):
		      guess+=u"n";

	    if guess in ("n","v"):
		    if guess=="n":
		        counter_noun+=1;
		    elif guess=="v":
		        counter_verb+=1;
		    counter_match+=1;
	    if guess=="vn":
	        guess=context_analyse(previous_word, word);
	        if guess!="vn": guess+="*";
	    print guess,
##	    print "\t",guess_stem(word).encode("utf8"),
	    print ;
	    previous_word=word;
	    counter+=1;
	print "nb match word",counter_match,"/",counter,"=", round(counter_match*100.0/counter,2),"%"
	print "stop words ",counter_stop_words,"reprsent ",round(counter_stop_words*100.0/counter,2) ,"%"
	print "found words + stop words ",counter_stop_words+counter_match,"=",round(counter_match*100.0/counter,2),"\%"
	print "found words percent on word without stop words ",round(counter_match*100.0/(counter-counter_stop_words),2),"\%"
	print "found verbs ",counter_verb, round(counter_verb*100.0/counter_match,2),"\%"
	print "found noun ",counter_noun, round(counter_noun*100.0/counter_match,2),"\%"

if __name__ == "__main__":
  main()








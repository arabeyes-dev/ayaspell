﻿from wordtag import *
tagger=WordTagger();

filename="../samples/xaa.txt";
filename="../samples/xaa.txt";
#filename="../samples/CCA_raw_utf8.txt";
try:
	myfile=open(filename)
	text=(myfile.read()).decode('utf8');
	if text==None:
		text=u""
except:
	text=u"أسلم"
ALS=tashaphyne.ArabicLightStemmer();
word_list=ALS.tokenize(text);

word_list=set(word_list);
word_count=len(word_list)
# word_list=(
# u'بينما',
# u'أو',
# u'البرنامج',
# u'الكذب',
# u'العربي',
# u'الصرفي',
# u'التطرف',
# )
def tag_word_list(word_list):
	counter={
	"vn":0,
	"=n=v":0,
	"!n!v":0,
	"v":0,
	"n":0,
	"t":0,
	"n1":0,
	"v1":0,
	"n2":0,
	};
	previous_word="";
	previous_tag="";

	for word in word_list:
		tag='';
		verbstamp="";
		stem="";
		rulecodeverb=0;
		rulecodenoun=0;
		if tagger.is_stopword(word):
			tag='t';
			#print "ok1"
		else:
			tagger.lightStemming(word);
			#stem=tagger.verbstemmer.get_stem();
			#verbstamp=tagger.verb_stamp(stem);
			rulecodenoun=tagger.is_possible_noun(word)
			#print rulecode;
			if rulecodenoun<=0:
				rulecodeverb=tagger.is_possible_verb(word);
			else:
				rulecodeverb=-10000;
			#if rulecodenoun>0:
			#	tag+='n';
			#if rulecodeverb>0:
			#	tag+='v';
			if rulecodenoun==0 and rulecodeverb==0:
				tag="vn";
			elif( rulecodenoun>=0 and rulecodeverb<0) or (rulecodenoun>0 and rulecodeverb==0):
				tag="n";
			elif (rulecodeverb>=0 and rulecodenoun<=0) or  (rulecodeverb>0 and rulecodenoun==0) :
				tag="v";
			elif (rulecodeverb<0 and rulecodenoun<0) :
				tag="!n!v";
			elif (rulecodeverb>0 and rulecodenoun>0) :
				tag="=n=v";
			if tag=='vn':
				#context tag
				tag=tagger.context_analyse(previous_word,word, previous_tag);
				if tag=='n': tag='n1';
				if tag=='v': tag='v1';

				if tag=='n': tag='n1';
				if tag=='v': tag='v1';
			counter[tag]+=1;
			previous_word=word;
			previous_tag=tag;
			#print rulecode;
	##    if tag in ("","nv"):
	##        tag=tagger.context_analyse(previous, word)+"1";
	##    list_result.append({'word':word,'tag': tag});
	##    previous=word;
		print '\t'.join([tag,str(rulecodeverb),str(rulecodenoun),word,stem,verbstamp]).encode('utf8');
	#statistics
	print "all words \t", word_count;
	for key in counter.keys():
		print "\t".join([str(key),str(counter[key]),str(counter[key]*100/word_count)+"\%"]);
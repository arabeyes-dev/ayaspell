﻿#!/usr/bin/python
# -*- coding=utf-8 -*-

import sys
import re
import string
import datetime
import  getopt
import os
from ar_adv import *

scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
def usage():
# "Display usage options"
	print "(C) CopyLeft 2009, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-V | --version]\tprogram version"
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname
	print "\r\nThis program is licensed under the GPL License\n"

def grabargs():
#  "Grab command-line arguments"
	all = False;
	segmentation=False;
	dictionarybased=False;
	templatesearch=False;
	rootextraction=False;
	vocalized=False;
	convert=False;
	fname = ''

	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hVdrtsv:f:",
                               ["help", "version","dict", "root", "template","all",
                                "seg", "vocalized", "file="],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-V", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-s", "--seg"):
			segmentation = True
		if o in ("-f", "--file"):
			fname = val
		if o in ("-d", "--dict"):
			dictionarybased = True
		if o in ("-r", "--root"):
			rootextraction=True;
		if o in ("-t", "--template"):

			templatesearch =True;
		if o in ("-v", "--vocalized"):
			vocalized =True;
		if o in ("-c", "--convert"):
			convert =True;
	return (fname,segmentation,dictionarybased,templatesearch,rootextraction,vocalized,convert)

def main():

	filename,segmentation,dictionarybased,templatesearch,rootextraction,vocalized,convert=grabargs()
	print "file name ",filename
#ToDo1 ref
	if (not filename):
		usage()
		sys.exit(0)
	option="";
	try:
		fl=open(filename);
	except :
		print " Error :No such file or directory: %s" % filename
		return None;
	line=fl.readline().decode("utf8");
	text=u""
	limit=12000;
	counter=0;
	counter_match=0;
	counter_stop_words=0;
	counter_stop_words_match=0;
	counter_verb=0;
	counter_noun=0;
#--------------------------------------------

	noun_prefix=u"مأسفلونيتاكب"
	noun_infix=u"اتويدط"
	noun_suffix=u"امتةكنهوي"
	noun_max_prefix=6
	noun_max_suffix=5
#--------------------------------------------

	while line and counter<limit:
##		text=" ".join([text,chomp(line)])
		line=fl.readline().decode("utf8");
	#print text.encode("utf8");
		listword=line.split(";");
		if len(listword)>=2:
		  word=listword[0];
		  word_nm=ar_strip_marks_keepshadda(word);
		  starword,left,right=transformToStars(word_nm,noun_prefix,noun_suffix,noun_infix,noun_max_prefix,noun_max_suffix);
		  starword=splitstarword(starword,left,right);
		  stem=word_nm[left:right];
		  guessed_type=u""
		  type_word_arabic=listword[1];

		  if type_word_arabic.find(u"فعل")>=0:
		      type_word=u"v"
		  elif type_word_arabic.find(u"اسم")>=0 :
		      type_word=u"n"
		  else:
		      type_word=u"t"
		      counter_stop_words+=1;
		      if is_stopword(word):
		          counter_stop_words_match+=1;
		          guessed_type=u"t"
		      else:
		          guessed_type=u"p"
		          print word.encode("utf8");

##		      guessed_type=u""
		  if type_word in("n","v"):
		      counter+=1;
		      word=word.strip(u" ")

##		      guessed_type=u""
		      code_return=0;
		      is_verb=is_possible_verb(word);
		      if is_verb>=0:
		          guessed_type+=u"v";
		      else:
		          code_return=is_verb;
		      is_noun=is_possible_noun(word)
		      if is_noun>=0:guessed_type+=u"n";
		      else:
		          code_return=is_noun;
		      if guessed_type=="":guessed_type="x"
		      if type_word==guessed_type:
		          counter_match+=1;
		      if guessed_type in ("n","v"):
			    if guessed_type=="n":counter_noun+=1;
			    elif guessed_type=="v":counter_verb+=1;
			    print word.encode("utf8"),u"\t",
			    print starword.encode("utf8"),u"\t",starword.count("*"),u"\t",
			    print u"\t".join([guessed_type,type_word,str(code_return)]),
			    print "\t",guess_stem(word).encode("utf8"),
			    print "\t",stem.encode("utf8"),
			    print u"\t",type_word_arabic.encode("utf8"),
		      if type_word not in guessed_type:
		          print word.encode("utf8"),u"\t",
		          print starword.encode("utf8"),u"\t",starword.count("*"),u"\t",

		          print u"\t".join([guessed_type,type_word,str(code_return)]),
		          print "\t",guess_stem(word).encode("utf8"),
		          print "\t",stem.encode("utf8"),
		          print u"\t",type_word_arabic.encode("utf8"),


		      if guessed_type=="vn":
		          print word.encode("utf8"),u"\t",
		          print starword.encode("utf8"),u"\t",starword.count("*"),u"\t",
		          print u"\t".join([guessed_type,type_word,"0"]),
		          print "\t",guess_stem(word).encode("utf8"),
		          print "\t",stem.encode("utf8"),
		          print u"\t",type_word_arabic.encode("utf8"),

	print ;
	print "--------------------",datetime.datetime.now(),"-------------------------"

	print "nb match word",counter_match,counter,round(counter_match*100.0/counter,2),"%"
	print "stop words ",counter_stop_words,"reprsent ",round(counter_stop_words*100.0/counter,2) ,"%"
	print "found words ",round(counter_match*100.0/(counter),2),"\%"
	print "found verbs ",counter_verb, round(counter_verb*100.0/counter_match,2),"\%"
	print "found noun ",counter_noun, round(counter_noun*100.0/counter_match,2),"\%"
	print "match stop word ",counter_stop_words_match

if __name__ == "__main__":
  main()








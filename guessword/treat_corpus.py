﻿#!/usr/bin/python
# -*- coding=utf-8 -*-
import sys
import re
import string
import time
import  getopt
import os
from ar_ctype import *
scriptname = os.path.splitext(os.path.basename(sys.argv[0]))[0]
scriptversion = '0.1'
AuthorName="Taha Zerrouki"
def usage():
# "Display usage options"
	print "(C) CopyLeft 2007, %s"%AuthorName
	print "Usage: %s -f filename [OPTIONS]" % scriptname
#"Display usage options"
	print "\t[-h | --help]\t\toutputs this usage message"
	print "\t[-V | --version]\tprogram version"
	print "\t[-f | --file= filename]\tinput file to %s"%scriptname
	print "\t[-s | --seg ]\t\tsegmentation"
	print "\t[-d | --dict]\tdictionary search"
	print "\t[-t | --template]\t\tTemplate search"
	print "\t[-r | --root]\t\troot extraction"
	print "\t[-v | --vocalized]\t\tuse vowel "
	print "\t[-c | --convert]\t\tconvert from translateration "
	print "\r\nThis program is licensed under the GPL License\n"

def grabargs():
#  "Grab command-line arguments"
	all = False;
	segmentation=False;
	dictionarybased=False;
	templatesearch=False;
	rootextraction=False;
	vocalized=False;
	convert=False;
	fname = ''

	if not sys.argv[1:]:
		usage()
		sys.exit(0)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hVdrtsv:f:",
                               ["help", "version","dict", "root", "template","all",
                                "seg", "vocalized", "file="],)
	except getopt.GetoptError:
		usage()
		sys.exit(0)
	for o, val in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit(0)
		if o in ("-V", "--version"):
			print scriptversion
			sys.exit(0)
		if o in ("-s", "--seg"):
			segmentation = True
		if o in ("-f", "--file"):
			fname = val
		if o in ("-d", "--dict"):
			dictionarybased = True
		if o in ("-r", "--root"):
			rootextraction=True;
		if o in ("-t", "--template"):

			templatesearch =True;
		if o in ("-v", "--vocalized"):
			vocalized =True;
		if o in ("-c", "--convert"):
			convert =True;
	return (fname,segmentation,dictionarybased,templatesearch,rootextraction,vocalized,convert)

def main():

	filename,segmentation,dictionarybased,templatesearch,rootextraction,vocalized,convert=grabargs()
	print "file name ",filename
#ToDo1 ref
	if (not filename):
		usage()
		sys.exit(0)
	option="";
	try:
		fl=open(filename);
	except IOerror:
		print " Error :No such file or directory: %s" % filename
		return None;
		try:
		  fl=open("../samples/corpus2.txt");
		except IOerror:
		  print " Error :No such file or directory: %s" % filename
		  return None;
	line=fl.readline().decode("utf8");
	text=u""
	limit=10000000;
	counter=0;
	counter_match=0;
	counter_stop_words=0;
	counter_verb=0;
	counter_noun=0;
	while line and counter<limit:
		line=fl.readline().decode("utf8");
		counter+=1;
##	#print text.encode("utf8");
		line=chomp(line).strip(" ");
		listword=line.split(" ");
		if len(listword)>=2:
			word=listword[1];
			stat=listword[0];
			word=word.strip(" ")
##			print
##			print  "\t",
			if is_valid_arabic_word(word):
			    t=word+u"\t"+stat;
			    print t.encode("utf8");
if __name__ == "__main__":
  main()







